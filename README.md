############################
LAND OR WATER
###########################

The script relies on the data from http://www.landcover.org/data/landcover/ and determines whether a point is on land or water based on its geographic coordinates.

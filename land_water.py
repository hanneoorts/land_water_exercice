import numpy as np
import matplotlib.pyplot as plt

NB_PIXELS = 43200
NB_LINES = 21600

def read_data():
    dat = np.fromfile('gl-latlong-1km-landcover.bsq', dtype = 'uint8')
    return dat.reshape((NB_LINES, NB_PIXELS))

def plot_map(array):
    plt.imshow(array)
    plt.show()


if __name__ == "__main__":
    print(np.shape(read_data()))
    data = read_data()
    plot_map(data[::50,::50])
